from __future__ import annotations

import logging
import os
from itertools import product

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import statsmodels.api as sm
from multiple_inference.bayes import Improper, Nonparametric, Normal
from multiple_inference.confidence_set import (
    AverageComparison,
    ConfidenceSet,
    PairwiseComparison
)
from multiple_inference.rank_condition import RankCondition
from scipy.stats import norm

from src.utils import CONVENTIONAL_ESTIMATES_DIR, RESULTS_DIR

logging.basicConfig(
    filename=os.path.join(RESULTS_DIR, "output.txt"), filemode="w", level=logging.INFO
)


def analyze(filename, compare_to_zero=False):
    def savefig(filename, suffix=""):
        if suffix:
            suffix = f"_{suffix}"
        plt.savefig(
            os.path.join(results_dir, f"{filename}{suffix}.png"),
            dpi=300,
            bbox_inches="tight",
        )

    def point_plot(filename, alphas=(0.05, 0.1), line_at_zero=False, **kwargs):
        for alpha in alphas:
            logging.info(f"Testing hypotheses with alpha={alpha}")
            if hasattr(results, "test_hypotheses"):
                logging.info(results.test_hypotheses(alpha=alpha))
            _, ax = plt.subplots(
                figsize=None if results.model.n_params < 40 else (7, 10)
            )
            results.point_plot(alpha=alpha, ax=ax, **kwargs)
            if line_at_zero:
                ax.axvline(0, linestyle="--")
            savefig(filename, f"alpha={alpha}")

    logging.info(f"Analyzing {filename}")
    if not os.path.exists(
        results_dir := os.path.join(RESULTS_DIR, filename[: -len(".csv")])
    ):
        os.mkdir(results_dir)

    full_filename = os.path.join(CONVENTIONAL_ESTIMATES_DIR, filename)
    columns = [
        col for col in pd.read_csv(full_filename).columns[1:] if col != "Control"
    ]
    conventional_results = Improper.from_csv(full_filename, columns=columns).fit()

    if compare_to_zero:
        logging.info("Compare to zero")
        results = ConfidenceSet.from_csv(full_filename, columns=columns).fit()
        logging.info(results.summary())
        for fdr in (0.05, 0.1):
            logging.info(
                f"Rejected {(results.qvalues<fdr).sum()} out of {len(results.qvalues)} hypotheses with FDR={fdr}"
            )
        point_plot("compare_to_zero", line_at_zero=True)

    logging.info("Pairwise comparisons")
    results = PairwiseComparison.from_csv(full_filename, columns=columns).fit()
    for fdr in (0.05, 0.1):
        logging.info(
            f"Rejected {(results.qvalues<fdr).sum()} out of {len(results.qvalues)} hypotheses with FDR={fdr}"
        )
    for alpha, criterion in product((0.05, 0.1), ("fwer", "fdr")):
        _, ax = plt.subplots(figsize=None if results.model.n_params < 40 else (10, 10))
        results.hypothesis_heatmap(
            alpha=alpha, criterion=criterion, triangular=True, axes=ax
        )
        savefig("pairwise_comparison", f"alpha={alpha}_criterion={criterion}")

    logging.info("Compare to average")
    results = AverageComparison.from_csv(full_filename, columns=columns).fit()
    logging.info(results.summary())
    for fdr in (0.05, 0.1):
        logging.info(
            f"Rejected {(results.qvalues<fdr).sum()} out of {len(results.qvalues)} hypotheses with FDR={fdr}"
        )
    point_plot("compare_to_average", line_at_zero=True)

    logging.info("Inference after ranking for largest estimated parameter")
    results = RankCondition.from_csv(full_filename, columns=columns).fit(beta=0.005)
    logging.info(f"Conventional point estimate: {conventional_results.params[0]}")
    logging.info(f"Hybrid point estimate: {results.params[0]}")
    logging.info(
        f"Conventional confidence interval: {conventional_results.conf_int(columns=0)}"
    )
    logging.info(f"Hybrid confidence interval: {results.conf_int(columns=0)}")

    logging.info("Parametric empirical Bayes")
    results = Normal.from_csv(full_filename, columns=columns).fit()
    logging.info(results.summary(robust=True, fast=True))
    point_plot("parametric_bayes", line_at_zero=compare_to_zero, robust=True, fast=True)
    for alpha in (.05, .1):
        logging.info(f"Selecting best parameters with alpha={alpha}")
        logging.info(results.compute_best_params(alpha=alpha))
        _, ax = plt.subplots(figsize=None if results.model.n_params < 40 else (7, 10))
        results.rank_point_plot(alpha=alpha, ax=ax)
        savefig("parametric_bayes_rank", f"alpha={alpha}")

    logging.info("Nonparametric empirical Bayes")
    results = Nonparametric.from_csv(full_filename, columns=columns).fit()
    logging.info(results.summary())
    point_plot("nonparametric_bayes", line_at_zero=compare_to_zero)
    for alpha in (.05, .1):
        logging.info(f"Selecting best parameters with alpha={alpha}")
        logging.info(results.compute_best_params(alpha=alpha))
        _, ax = plt.subplots(figsize=None if results.model.n_params < 40 else (7, 10))
        results.rank_point_plot(alpha=alpha, ax=ax)
        savefig("nonparametric_bayes_rank", f"alpha={alpha}")


def analyze_judges(filename, n_samples=200):
    logging.info(f"Analyzing {filename}")
    for model_cls in (Improper, Nonparametric):
        logging.info(f"Model class: {str(model_cls)}")
        model = model_cls.from_csv(os.path.join(CONVENTIONAL_ESTIMATES_DIR, filename))
        results = model.fit(n_samples=n_samples)
        distribution = np.exp(results._posterior_rvs)
        mean, std = distribution.mean(), distribution.std(axis=1).mean()
        for n_stds in (0, 1, 2):
            logging.info(f"Mean discrimination + {n_stds} stds: {mean + n_stds * std}")
        logging.info(
            f"Pr max discrimination > 2x: {(distribution.max(axis=1) > 2).mean()}"
        )
        logging.info(
            f"Pr max discrimination > 3x: {(distribution.max(axis=1) > 3).mean()}"
        )


def analyze_forecasters():
    filename = os.path.join(CONVENTIONAL_ESTIMATES_DIR, "superforecasters.csv")

    # conventional estimates and CIs
    model = Improper.from_csv(filename)
    # top 2% of forecasters are "superforecasters"
    n_superforecasters = int(0.02 * model.n_params)
    conventional_mean = model.mean[:n_superforecasters]
    conventional_ci = np.expand_dims(conventional_mean, -1) + np.array(
        [
            norm(scale=np.sqrt(variance)).ppf([0.025, 0.975])
            for variance in model.cov.diagonal()[:n_superforecasters]
        ]
    )

    # Bayesian estimates and CIs
    bayes_model = Nonparametric.from_csv(filename)
    means, conf_ints = [], []
    for i in range(n_superforecasters):
        dist = bayes_model.get_marginal_distribution(i)
        means.append(dist.mean())
        conf_ints.append(dist.ppf([0.025, 0.975]))
    bayes_mean, bayes_ci = np.array(means), np.array(conf_ints)

    # hybrid estimates and CIs
    model = RankCondition.from_csv(filename)
    medians, conf_ints = [], []
    for i in range(n_superforecasters):
        dist = model.get_marginal_distribution(
            i, ranks=np.arange(n_superforecasters), beta=0.005
        )
        medians.append(dist.ppf(0.5))
        conf_ints.append(dist.ppf([0.025, 0.975]))
    hybrid_median, hybrid_ci = np.array(medians), np.array(conf_ints)

    # plot
    def plot(mean, conf_int, yticks, label):
        ax.errorbar(
            x=mean,
            y=yticks,
            xerr=[mean - conf_int[:, 0], conf_int[:, 1] - mean],
            fmt="o",
            label=label,
        )

    fig, ax = plt.subplots(figsize=(7, 10))
    yticks = 4 * np.arange(n_superforecasters, 0, -1)
    plot(conventional_mean, conventional_ci, yticks, "Conventional")
    plot(bayes_mean, bayes_ci, yticks - 1, "Bayes")
    plot(hybrid_median, hybrid_ci, yticks - 2, "Hybrid")
    ax.axvline(0, linestyle="--")
    ax.set_yticks(yticks - 1)
    ax.set_yticklabels([f"forecaster {i+1}" for i in range(n_superforecasters)])
    ax.set_xlabel("Standard deviations better than average (Brier score)")
    ax.set_ylabel("Superforecasters")
    fig.legend()

    # save figure
    if not os.path.exists(results_dir := os.path.join(RESULTS_DIR, "forecasters")):
        os.mkdir(results_dir)
    fig.savefig(
        os.path.join(results_dir, "superforecasters_estimates.png"), dpi=300, bbox_inches="tight"
    )

    logging.info("Analyzing superforecasters")
    logging.info(f"Shrinkage to 0: {(bayes_mean / conventional_mean).mean()}")

    # compute number of selected superforecasters
    results = bayes_model.fit()
    df = []
    for q, alpha, superset, criterion in product((.02, .2), (.1, .2), (True, False), ("fdr", "fwer")):
        selected = results.compute_best_params(int(q * results.model.n_params), alpha=alpha, superset=superset, criterion=criterion)
        df.append(
            {
                "Quantile": q,
                "Error rate": alpha,
                "Set": "Superset" if superset else "Subset",
                "Criterion": "FDR" if criterion == "fdr" else "FWER",
                "Percent selected": selected.mean()
            }
        )
    df = pd.DataFrame(df)
    g = sns.FacetGrid(data=df, col="Set", row="Criterion", sharey=False)
    g.map_dataframe(sns.barplot, x="Quantile", y="Percent selected", hue="Error rate", palette=sns.color_palette())
    g.add_legend()
    plt.savefig(os.path.join(results_dir, "superforecasters_selected.png"), bbox_inches="tight", dpi=300)
    df.to_csv(os.path.join(results_dir, "superforecasters.csv"))


def analyze_diversity():
    """Analyze the diversity study."""
    logging.info("Analyzing diversity")
    if not os.path.exists(results_dir := os.path.join(RESULTS_DIR, "diversity")):
        os.mkdir(results_dir)

    logging.info("Comparing to the average")
    filename = os.path.join(CONVENTIONAL_ESTIMATES_DIR, "diversity.csv")
    columns = pd.read_csv(filename).columns[1:]
    agreed_cols = np.array([col.startswith("Agreed") for col in columns], dtype=int)
    results = AverageComparison.from_csv(filename, X=sm.add_constant(agreed_cols)).fit()
    logging.info(results.summary())

    for alpha in (0.05, 0.1):
        logging.info(f"Analyzing with alpha={alpha}")
        logging.info(results.test_hypotheses(alpha=alpha))
        fig, axes = plt.subplots(2, 1, figsize=(7, 10))
        for ax, cols, xlabel in zip(
            axes,
            (agreed_cols, 1 - agreed_cols),
            ("Pct. agreed to meet", "Pct. responded"),
        ):
            results.point_plot(alpha=alpha, columns=cols.astype(bool), ax=ax)
            ax.axvline(0, linestyle="--")
            ax.set_xlabel(xlabel)
            ax.set_title(None)
        fig.savefig(
            os.path.join(results_dir, f"compare_to_average_alpha_{alpha}.png"),
            bbox_inches="tight",
            dpi=300,
        )

    logging.info("Pairwise comparisons")
    groups = ["Agreed" if col.startswith("Agreed") else "Responded" for col in columns]
    results = PairwiseComparison.from_csv(filename).fit(groups=groups)
    for alpha, criterion in product((0.05, 0.1), ("fwer", "fdr")):
        logging.info(f"Analyzing with alpha={alpha}")
        fig, axes = plt.subplots(2, figsize=(10, 25))
        results.hypothesis_heatmap(
            alpha=alpha, criterion=criterion, axes=axes, triangular=True
        )
        fig.savefig(
            os.path.join(
                results_dir,
                f"pairwise_comparisons_alpha={alpha}_criterion={criterion}.png",
            ),
            bbox_inches="tight",
            dpi=300,
        )


if __name__ == "__main__":
    sns.set()
    np.random.seed(0)

    filenames = (
        ("diversity_agreed.csv", False),
        ("diversity_responded.csv", False),
        ("exercise.csv", True),
        ("penn.csv", False),
        ("predictive_traits_agree_with.csv", True),
        ("predictive_traits_heard_of.csv", True),
        ("state_reps.csv", False),
        ("tax.csv", False),
        ("walmart.csv", False),
    )
    for filename, compare_to_zero in filenames:
        analyze(filename, compare_to_zero)
        plt.close("all")

    analyze_judges("judges_black.csv")
    analyze_judges("judges_hispanic.csv")
    plt.close("all")

    analyze_forecasters()
    plt.close("all")
    analyze_diversity()
