from __future__ import annotations

import os

import numpy as np
import pandas as pd

from src.utils import (
    JUDGES_DATA_DIR,
    PREDICTIVE_TRAITS_DATA_DIR,
    FORECASTING_DATA_DIR,
    WALMART_DATA_DIR,
    PROCESSED_DATA_DIR,
)


def clean_judges():
    # cleaning procedure follows that of the original paper
    # see https://osf.io/vnuwb/ file interjudge.R
    df = pd.read_csv(
        os.path.join(JUDGES_DATA_DIR, "FinalDataset_2001-2019.csv"), low_memory=False
    )
    # start after 2005
    start_year = 2005
    df = df[df.SENTYR > start_year]

    # select non-immigration offenses
    immigration_offtype = 27
    immigration_offguide = 17
    df = df[
        (df.OFFTYPE2 != immigration_offtype) & (df.OFFGUIDE != immigration_offguide)
    ]

    # exclude cases where the sentence total is listed as 0 but the type of sentence given supposedly includes prison
    important_sentences = [1, 2]
    df = df[~((df.SENTTOT == 0) & df.SENTIMP.isin(important_sentences))]

    # exclude the one case resulting in the death penalty
    death_penalty_senttot = 9998
    df = df[df.SENTTOT != death_penalty_senttot]

    # create an indicator for government sponsorship
    reason_variables = [f"REAS{i}" for i in range(1, 4)]
    govspon_reasons = [19, 500, 48]
    df["GOVSPON"] = (
        df[reason_variables].isin(govspon_reasons).any(axis=1).fillna(0).astype(int)
    )

    # create an indicator for new family
    new_family_reason = 17
    df["FAMILY"] = (
        (df[reason_variables] == new_family_reason).any(axis=1).fillna(0).astype(int)
    )

    # create female dummy
    female_value = 1
    df["FEMALE"] = (df.MONSEX == female_value).astype(int)

    # create race dummies
    race_map = {"HISP": 3, "WHITE": 1, "BLACK": 2, "OTHERRACE": 6}
    race_dummy_names = list(race_map.keys())
    for race_name, race_value in race_map.items():
        df[race_name] = (df.NEWRACE == race_value).astype(int)

    # create dummies for crime categories
    def make_offense_indicator(offense_name, offtype_range, offguide_values):
        df[offense_name] = (
            (df.OFFTYPE2.between(*offtype_range) | df.OFFGUIDE.isin(offguide_values))
            .fillna(0)
            .astype(int)
        )

    violent_offense_range = 1, 7
    violent_guide_values = 3, 4, 19, 20, 22, 26, 27
    make_offense_indicator("VIOLENTCRIME", violent_offense_range, violent_guide_values)

    drug_offense_range = 10, 12
    drug_guide_values = [9, 10]
    make_offense_indicator("DRUGS", drug_offense_range, drug_guide_values)

    eft_offense_range = 15, 24
    eft_guide_values = 5, 6, 12, 15, 16, 21, 29
    make_offense_indicator("EFT", eft_offense_range, eft_guide_values)

    # create dummy for U.S. citizen
    df["USCITIZEN"] = (df.NEWCIT == 0).astype(int)

    # bin the sentencing year
    n_year_bins = 4
    df["SENTYR_BIN"] = pd.qcut(df.SENTYR, q=n_year_bins)

    # create dummy variables
    dummy_columns = ["NEWEDUC", "NEWCNVTN", "SENTYR_BIN"]
    df = pd.get_dummies(df, columns=dummy_columns, drop_first=True)

    dummy_variables = []  # holds the names of the new dummy variables
    for col in df.columns:
        for dummy_prefix in dummy_columns:
            if col.startswith(dummy_prefix):
                dummy_variables.append(col)
                break

    # create outcome variable
    max_sentence_length = 470
    df["LOG_SENTENCE_LENGTH"] = np.log(df.SENTTOT.clip(None, max_sentence_length) + 1)

    # select relevant variables for analysis
    control_variables = [
        "GLMIN",
        "CRIMPTS",
        "GOVSPON",
        "FAMILY",
        "FEMALE",
        "AGE",
        "USCITIZEN",
        "VIOLENTCRIME",
        "DRUGS",
        "EFT",
    ]
    df = df[
        ["LOG_SENTENCE_LENGTH", "judge_clean"]
        + list(race_map.keys())
        + control_variables
        + dummy_variables
    ]
    df = df.dropna()

    # count number of cases of each race for each judge
    n_cases_transformed = df.groupby("judge_clean")[race_dummy_names].transform("sum")
    # keep only judges with at least 25 white defendants and at least 25 black or 25 hispanic defendants
    min_cases = 25
    df = df[
        (n_cases_transformed.WHITE >= min_cases)
        & (
            (n_cases_transformed.BLACK >= min_cases)
            | (n_cases_transformed.HISP >= min_cases)
        )
    ]
    n_cases_df = df.groupby("judge_clean")[["BLACK", "HISP"]].sum().reset_index()
    n_cases_df.to_csv(
        os.path.join(PROCESSED_DATA_DIR, "judges_n_cases.csv"), index=False
    )
    # drop white race dummy to avoid multicollinearity (white is the comparison group)
    df = df.drop(columns="WHITE")
    df.to_csv(os.path.join(PROCESSED_DATA_DIR, "judges.csv"), index=False)


def clean_predictive_traits():
    # cleaning procedure follows https://osf.io/pnufh/
    df = pd.read_csv(
        os.path.join(
            PREDICTIVE_TRAITS_DATA_DIR, "student_values_survey_2022_NYU_anonymized.csv"
        )
    )
    df["Heard of Effective Altruism"] = (df.heardof_1 == 1).astype(int)
    # exclude students who failed the attention check or took too long
    df = df[(df.attention == 5) & (df["Duration (in seconds)"] > 420)]
    # indicates cis-female
    df["Female"] = df.gender.apply(lambda x: int(x == 1) if x in (1, 2) else np.nan)
    # SAT of <400 is impossible
    df.sat[df.sat < 400] = np.nan
    # clean majors
    dental_majors = (
        "dental",
        "dentistry",
        "dental school",
        "dental sciences",
        "dental student",
        "dentistry (graduate)",
    )
    df["major_44"] = df.major_42_TEXT.str.lower().isin(
        dental_majors
    ) | df.major_43_TEXT.str.lower().isin(dental_majors)
    majors = {
        "Arts and media major": range(1, 9),
        "Business major": (9,),
        "Education major": (10,),
        "Health major": (11, 12, 13, 44),
        "Humanities major": (16, 17, 18, 19, 20, 23),
        "Social science major": range(25, 30),
        "STEM major": (31, 32, 33, 34, 35, 36, 37, 39, 40, 41, 42),
    }
    for major_name, major_values in majors.items():
        df[major_name] = (
            (df[[f"major_{i}" for i in major_values]] == 1).any(axis=1).astype(int)
        )

    # rename columns to avoid direction ambiguity
    df = df.rename(
        columns={
            "political": "Politically conservative",
            "agree_disagree": "Agree with Effective Altruism",
            "sat": "SAT score",
            "age": "Age",
            "religious": "Religiosity",
        }
    )

    df[
        [
            "Heard of Effective Altruism",
            "Agree with Effective Altruism",
            "Female",
            "SAT score",
            "Age",
            "Politically conservative",
            "Religiosity",
            *majors.keys(),
        ]
    ].to_csv(os.path.join(PROCESSED_DATA_DIR, "predictive_traits.csv"), index=False)


def clean_superforecasters():
    problems_df = pd.read_csv(os.path.join(FORECASTING_DATA_DIR, "ifps.csv"))
    forecasts_df = pd.read_csv(
        os.path.join(FORECASTING_DATA_DIR, "survey_fcasts.yr1.csv")
    )
    forecasts_df = forecasts_df.drop_duplicates(["ifp_id", "user_id", "answer_option"])
    df = problems_df.merge(forecasts_df, on="ifp_id").dropna(subset="user_id")

    # compute Brier score
    df["squared_error"] = (df.value - (df.answer_option == df.outcome)) ** 2
    df["neg_brier_score"] = -df.groupby(["ifp_id", "user_id"]).squared_error.transform(
        "mean"
    )
    df = df.drop_duplicates(["ifp_id", "user_id"])

    # drop forecasters who participated in fewer than half the problems
    n_ifps = len(df.ifp_id.unique())
    df = df[df.groupby("user_id").ifp_id.transform("count") >= n_ifps / 2]

    # if a forecaster did not predict for a question, fill the missing value with the
    # mean Brier score for that question
    df = df.pivot("ifp_id", "user_id", "neg_brier_score")
    fill_values = np.repeat(np.atleast_2d(df.mean(axis=1)).T, len(df.columns), axis=1)
    df = df.fillna(0) + (df.isna() * fill_values)

    # reshape to long
    df = df.rename(columns={i: f"neg_brier_score{int(i)}" for i in df.columns})
    df = pd.wide_to_long(
        df.reset_index(), "neg_brier_score", "ifp_id", "user_id"
    ).reset_index()
    df.to_csv(os.path.join(PROCESSED_DATA_DIR, "superforecasters.csv"), index=False)


def clean_walmart():
    # clean the Walmart flu study estimates
    pct_vaccinated_control = 0.294  # from PNAS submission
    gist_df = pd.read_csv(
        os.path.join(WALMART_DATA_DIR, "WMTAttributes_Analysis_Data1and3mo.csv")
    )
    df = pd.read_csv(os.path.join(WALMART_DATA_DIR, "dec_estsmat_js.csv"))
    df.term = df.term.str[len("condition_name") :].str.lstrip("0")
    df = df.merge(gist_df, left_on="term", right_on="Intervention")
    df = df.set_index("Gist")[["N", "estimate"]]
    df.estimate += pct_vaccinated_control
    # 689693 participants in total
    df = pd.concat(
        [
            df,
            pd.DataFrame(
                {"N": [689693 - df.N.sum()], "estimate": [pct_vaccinated_control]},
                index=["Control"],
            ),
        ]
    )
    df["n_vaccinated"] = round(df.N * df.estimate)
    y = np.concatenate(
        df.apply(
            lambda row: int(row.n_vaccinated) * [1]
            + int(row.N - row.n_vaccinated) * [0],
            axis=1,
        )
    )
    arm = np.concatenate(df.apply(lambda row: int(row.N) * [row.name], axis=1))
    pd.DataFrame({"y": y, "arm": arm}).to_csv(
        os.path.join(PROCESSED_DATA_DIR, "walmart.csv"), index=False
    )


if __name__ == "__main__":
    clean_judges()
    clean_predictive_traits()
    clean_superforecasters()
    clean_walmart()
