from __future__ import annotations

import os

if not os.path.exists(DATA_DIR := "data"):
    os.mkdir(DATA_DIR)
if not os.path.exists(RAW_DATA_DIR := os.path.join(DATA_DIR, "raw")):
    os.mkdir(RAW_DATA_DIR)
if not os.path.exists(EXTERNAL_DATA_DIR := os.path.join(DATA_DIR, "external")):
    os.mkdir(EXTERNAL_DATA_DIR)
if not os.path.exists(PROCESSED_DATA_DIR := os.path.join(DATA_DIR, "processed")):
    os.mkdir(PROCESSED_DATA_DIR)
if not os.path.exists(
    CONVENTIONAL_ESTIMATES_DIR := os.path.join(DATA_DIR, "conventional_estimates")
):
    os.mkdir(CONVENTIONAL_ESTIMATES_DIR)

if not os.path.exists(JUDGES_DATA_DIR := os.path.join(EXTERNAL_DATA_DIR, "judges")):
    os.mkdir(JUDGES_DATA_DIR)
if not os.path.exists(
    PREDICTIVE_TRAITS_DATA_DIR := os.path.join(EXTERNAL_DATA_DIR, "predictive_traits")
):
    os.mkdir(PREDICTIVE_TRAITS_DATA_DIR)
if not os.path.exists(
    FORECASTING_DATA_DIR := os.path.join(EXTERNAL_DATA_DIR, "superforecasters")
):
    os.mkdir(FORECASTING_DATA_DIR)
if not os.path.exists(
    STATE_REPS_DATA_DIR := os.path.join(EXTERNAL_DATA_DIR, "state_reps")
):
    os.mkdir(STATE_REPS_DATA_DIR)
if not os.path.exists(WALMART_DATA_DIR := os.path.join(EXTERNAL_DATA_DIR, "walmart")):
    os.mkdir(WALMART_DATA_DIR)
if not os.path.exists(PENN_DATA_DIR := os.path.join(EXTERNAL_DATA_DIR, "penn")):
    os.mkdir(PENN_DATA_DIR)
if not os.path.exists(
    DIVERSITY_DATA_DIR := os.path.join(EXTERNAL_DATA_DIR, "diversity")
):
    os.mkdir(DIVERSITY_DATA_DIR)

if not os.path.exists(RESULTS_DIR := "results"):
    os.mkdir(RESULTS_DIR)
