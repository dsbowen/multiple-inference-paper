from __future__ import annotations

import os

import numpy as np
import pandas as pd
import statsmodels.api as sm
from multiple_inference.base import ModelBase

from src.utils import (
    CONVENTIONAL_ESTIMATES_DIR,
    DIVERSITY_DATA_DIR,
    PENN_DATA_DIR,
    PROCESSED_DATA_DIR,
    STATE_REPS_DATA_DIR,
    WALMART_DATA_DIR,
)


def estimate_judges(min_cases=25):
    def run_regression(judge_name, df):
        X = sm.add_constant(df.drop(columns=["LOG_SENTENCE_LENGTH", "judge_clean"]))
        results = sm.OLS(df.LOG_SENTENCE_LENGTH, X).fit(cov_type="HC3")
        return dict(
            judge_clean=judge_name,
            black_param=results.params["BLACK"],
            black_var=results.cov_params()["BLACK"]["BLACK"],
            hispanic_param=results.params["HISP"],
            hispanic_var=results.cov_params()["HISP"]["HISP"],
        )

    df = pd.read_csv(os.path.join(PROCESSED_DATA_DIR, "judges.csv"))
    n_cases_df = pd.read_csv(os.path.join(PROCESSED_DATA_DIR, "judges_n_cases.csv"))
    results_df = pd.DataFrame(
        [
            run_regression(judge_name, judge_df)
            for judge_name, judge_df in list(df.groupby("judge_clean"))
        ]
    )
    results_df = results_df.merge(n_cases_df, on="judge_clean").set_index("judge_clean")

    black_results_df = results_df[
        (results_df.BLACK >= min_cases) & (results_df.black_var < np.inf)
    ]
    model = ModelBase(
        black_results_df.black_param, np.diag(black_results_df.black_var), sort=True
    )
    model.to_csv(os.path.join(CONVENTIONAL_ESTIMATES_DIR, "judges_black.csv"))

    hispanic_results_df = results_df[
        (results_df.HISP >= min_cases) & (results_df.hispanic_var < np.inf)
    ]
    model = ModelBase(
        hispanic_results_df.hispanic_param,
        np.diag(hispanic_results_df.hispanic_var),
        sort=True,
    )
    model.to_csv(os.path.join(CONVENTIONAL_ESTIMATES_DIR, "judges_hispanic.csv"))


def estimate_predictive_traits():
    df = pd.read_csv(os.path.join(PROCESSED_DATA_DIR, "predictive_traits.csv"))
    correlates = [
        col
        for col in df.columns
        if col not in ("Agree with Effective Altruism", "Heard of Effective Altruism")
    ]
    agree_with_correlations, heard_of_correlations = [], []
    compute_correlation = lambda df, variable: [
        df[[variable, correlate]].corr()[variable][correlate]
        for correlate in correlates
    ]
    for _ in range(10000):
        # get bootstrapped distribution of correlation coefficients
        df_sample = df.sample(frac=1, replace=True)
        agree_with_correlations.append(
            compute_correlation(df_sample, "Agree with Effective Altruism")
        )
        heard_of_correlations.append(
            compute_correlation(df_sample, "Heard of Effective Altruism")
        )

    model = ModelBase(
        compute_correlation(df, "Agree with Effective Altruism"),
        np.cov(np.array(agree_with_correlations).T),
        exog_names=correlates,
        endog_names="Agree with Effective Altruism",
        sort=True,
    )
    model.to_csv(
        os.path.join(CONVENTIONAL_ESTIMATES_DIR, "predictive_traits_agree_with.csv")
    )

    model = ModelBase(
        compute_correlation(df, "Heard of Effective Altruism"),
        np.cov(np.array(heard_of_correlations).T),
        exog_names=correlates,
        endog_names="Heard of Effective Altruism",
        sort=True,
    )
    model.to_csv(
        os.path.join(CONVENTIONAL_ESTIMATES_DIR, "predictive_traits_heard_of.csv")
    )


def estimate_superforecasters():
    df = pd.read_csv(os.path.join(PROCESSED_DATA_DIR, "superforecasters.csv"))
    # standardize the negative Brier scores
    std = df.groupby("user_id").neg_brier_score.mean().std()
    df.neg_brier_score = (df.neg_brier_score - df.neg_brier_score.mean()) / std
    results = (
        sm.OLS(df.neg_brier_score, pd.get_dummies(df.user_id))
        .fit()
        .get_robustcov_results(cov_type="cluster", groups=df.ifp_id)
    )
    model = ModelBase.from_results(results, sort=True)
    model.to_csv(os.path.join(CONVENTIONAL_ESTIMATES_DIR, "superforecasters.csv"))


def estimate_state_reps():
    df = pd.read_csv(os.path.join(STATE_REPS_DATA_DIR, "state_reps.csv"))
    df = df.set_index("state")
    n_calls_per_state = 40
    df["pr_reach_representative"] = df.n_reach_representative / n_calls_per_state
    cov = np.diag(
        df.pr_reach_representative
        * (1 - df.pr_reach_representative)
        / n_calls_per_state
    )
    model = ModelBase(
        1-df.pr_reach_representative,
        cov,
        sort=True,
        endog_names="Pr. not reaching a live representative",
    )
    model.to_csv(os.path.join(CONVENTIONAL_ESTIMATES_DIR, "state_reps.csv"))


def estimate_walmart():
    pred_df = pd.read_csv(os.path.join(WALMART_DATA_DIR, "WMT_PI_Clean.csv"))
    pct_vaccinated_control = pred_df[
        pred_df.Condition == "Control"
    ].PercVaccinated.iloc[0]
    gist_df = pd.read_csv(
        "data/external/walmart/WMTAttributes_Analysis_Data1and3mo.csv"
    )

    # means are the estimated effects; need to add control vaccination rate
    mean = pd.read_csv("data/external/walmart/dec_estsmat_js.csv")
    mean.term = mean.term.str[len("condition_name") :].str.lstrip("0")
    mean = mean.merge(gist_df, left_on="term", right_on="Intervention")
    mean = mean.set_index("Gist").estimate
    mean += pct_vaccinated_control
    mean = pd.concat([mean, pd.Series([pct_vaccinated_control], index=["Control"])])

    # similarly, need to adjust covariance matrix to reflect vaccination rates
    cov = pd.read_csv("data/external/walmart/dec_vcov_js.csv").values[:, 1:]
    var_control = cov[0, 1]
    cov = np.diag(cov.diagonal() - cov[0, 1])
    cov = np.vstack(
        [np.hstack([cov, np.zeros((len(cov), 1))]), np.zeros((1, len(cov) + 1))]
    )
    cov[-1, -1] = var_control

    model = ModelBase(mean, cov, sort=True)
    model.to_csv(os.path.join(CONVENTIONAL_ESTIMATES_DIR, "walmart.csv"))

    # save treatment features
    X = np.vectorize(lambda x: "3 d later" in x)(model.exog_names).astype(int)
    X = pd.DataFrame(
        sm.add_constant(X), columns=["const", "multi_day"], index=model.exog_names
    )
    X.to_csv(os.path.join(CONVENTIONAL_ESTIMATES_DIR, "walmart_features.csv"))


def estimate_penn():
    df = pd.read_csv(os.path.join(PENN_DATA_DIR, "final-results.csv"))
    control_vaccination_rate = 0.42
    df.Estimate += control_vaccination_rate
    control_variance = df.SE.mean() ** 2 / 2
    df["variance"] = df.SE**2 - control_variance
    df = pd.concat(
        [
            df,
            pd.DataFrame(
                {
                    "condition": "Control",
                    "Estimate": [control_vaccination_rate],
                    "variance": [control_variance],
                }
            ),
        ]
    )
    df = df.set_index("condition")
    model = ModelBase(df.Estimate, np.diag(df.variance), sort=True)
    model.to_csv(os.path.join(CONVENTIONAL_ESTIMATES_DIR, "penn.csv"))


def estimate_diversity():
    def get_demographic(row):
        student_race = None
        for race in ("caucasian", "black", "hispanic", "chinese", "indian"):
            if row[f"stud_{race}"]:
                student_race = race
                break
        student_sex = "female" if row[f"stud_{student_race}_x_female"] else "male"
        return f"{student_race.capitalize()} {student_sex}"

    df = pd.read_excel(
        os.path.join(DIVERSITY_DATA_DIR, "20140623_Shareable.xls"), "data"
    )

    # create indicators for target (response received or agreed to meet)
    # and demographics (race and gender)
    df["demographic"] = df.apply(get_demographic, axis=1)
    df = df.rename(
        columns={
            "agreed_to_mtg_date": "target Agreed",
            "response_received": "target Responded",
        }
    )
    df["email"] = df.index
    df = pd.wide_to_long(
        df, "target", "email", "target_type", sep=" ", suffix="\w+"
    ).reset_index()
    df["target_demographic"] = df.apply(
        lambda row: f"{row.target_type}, {row.demographic}", axis=1
    )

    # estimate models
    results = sm.OLS(df.target, pd.get_dummies(df.target_demographic)).fit(
        cov_type="cluster", cov_kwds={"groups": df.email}
    )
    model = ModelBase.from_results(results, sort=True)
    model.to_csv(os.path.join(CONVENTIONAL_ESTIMATES_DIR, "diversity.csv"))

    agreed_df = df[df.target_type == "Agreed"]
    results = sm.OLS(agreed_df.target, pd.get_dummies(agreed_df.demographic)).fit(
        cov_type="HC3"
    )
    model = ModelBase.from_results(
        results, sort=True, endog_names="Pct. agreed to meet"
    )
    model.to_csv(os.path.join(CONVENTIONAL_ESTIMATES_DIR, "diversity_agreed.csv"))

    responded_df = df[df.target_type == "Responded"]
    results = sm.OLS(responded_df.target, pd.get_dummies(responded_df.demographic)).fit(
        cov_type="HC3"
    )
    model = ModelBase.from_results(results, sort=True, endog_names="Pct. responded")
    model.to_csv(os.path.join(CONVENTIONAL_ESTIMATES_DIR, "diversity_responded.csv"))


if __name__ == "__main__":
    np.random.seed(0)
    estimate_judges()
    estimate_predictive_traits()
    estimate_superforecasters()
    estimate_state_reps()
    estimate_walmart()
    estimate_penn()
    estimate_diversity()
