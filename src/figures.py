from __future__ import annotations

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import patches
from multiple_inference.bayes import Improper, Normal, Nonparametric
from multiple_inference.confidence_set import ConfidenceSet, PairwiseComparison
from scipy.stats import norm, multivariate_normal

from src.utils import RESULTS_DIR, CONVENTIONAL_ESTIMATES_DIR

COLOR_PALETTE = sns.color_palette()

if not os.path.exists(RESULTS_DIR := os.path.join(RESULTS_DIR, "figures")):
    os.mkdir(RESULTS_DIR)


def make_qvalues_figure():
    n_params = 500
    # draw n_params estimates from a null distribution (mean == 0)
    # and n_params estimates from an alternative distribution (mean == 1)
    mean = np.concatenate([norm.rvs(size=n_params), norm.rvs(1.5, size=n_params)])
    results = ConfidenceSet(mean, np.identity(2 * n_params)).fit()
    # compute conventional pvalues
    pvalues = norm.cdf(results.model.mean)
    pvalues = 2 * np.min([pvalues, 1 - pvalues], axis=0)
    index = pvalues.argmax()
    true_null_rate = results.qvalues[index] / pvalues[index]
    fig, axes = plt.subplots(3, figsize=(7, 15))
    histplot = lambda ax: sns.histplot(pvalues, stat="density", bins=10, ax=ax)
    histplot(axes[0])
    histplot(axes[1])
    axes[1].axhline(true_null_rate, linestyle="--", color=COLOR_PALETTE[1])
    histplot(axes[2])
    axes[2].axhline(true_null_rate, linestyle="--", color=COLOR_PALETTE[1])
    axes[2].axvline(0.1, linestyle="--", color=COLOR_PALETTE[1])
    axes[2].bar(
        0.05,
        (pvalues < 0.1).mean() / 0.1,
        width=0.1,
        color=COLOR_PALETTE[2],
        edgecolor="white",
    )
    axes[2].bar(
        0.05,
        results.qvalues[index] / pvalues[index],
        width=0.1,
        color=COLOR_PALETTE[3],
        edgecolor="white",
    )
    plt.savefig(os.path.join(RESULTS_DIR, "qvalues.png"), bbox_inches="tight", dpi=300)


def make_simultaneous_ci_figure():
    x = multivariate_normal.rvs(mu := np.array([0, 1]), cov := np.identity(2) + 0.1)
    conf_int = ConfidenceSet(x, cov).fit().conf_int()
    conf_int_diff = np.diff(conf_int, axis=1)[:, 0]
    padding = 0.1 * conf_int_diff
    fig, ax = plt.subplots()
    ax.add_patch(
        patches.Rectangle(
            conf_int[:, 0],
            conf_int_diff[0],
            conf_int_diff[1],
            fill=False,
            edgecolor=sns.color_palette()[0],
            linewidth=3,
        )
    )
    ax.set_xlim((conf_int[0, 0] - padding[0], conf_int[0, 1] + padding[0]))
    ax.set_ylim((conf_int[1, 0] - padding[1], conf_int[1, 1] + padding[1]))
    ax.axvline(x[0], 0, 0.5, linestyle="--")
    ax.axhline(x[1], 0, 0.5, linestyle="--")
    ax.add_patch(
        patches.FancyArrowPatch(
            (conf_int[0, 0], conf_int[1, 1] - 2 * padding[1]),
            (conf_int[0, 1], conf_int[1, 1] - 2 * padding[1]),
            arrowstyle="<->, head_width=3, head_length=3",
            linewidth=2,
            color=sns.color_palette()[1],
        )
    )
    ax.text(
        x[0],
        conf_int[1, 1] - 2 * padding[1],
        "Simultaneous confidence interval for parameter 1",
        verticalalignment="bottom",
        horizontalalignment="center",
    )
    ax.scatter(x[0], x[1])
    ax.text(x[0], x[1], r"$(m_1, m_2)$")
    ax.scatter(mu[0], mu[1], color=sns.color_palette()[0])
    ax.text(mu[0], mu[1], r"$(\mu_1, \mu_2)$")
    ax.set_xlabel("Parameter 1")
    ax.set_ylabel("Parameter 2")
    plt.savefig(
        os.path.join(RESULTS_DIR, "simultaneous_ci.png"), bbox_inches="tight", dpi=300
    )


def make_pairwise_figure():
    results = PairwiseComparison.from_csv(
        os.path.join(CONVENTIONAL_ESTIMATES_DIR, "exercise.csv"),
        columns=[1, 11, 28, 50],
    ).fit()
    fig, ax = plt.subplots()
    ax = results.point_plot(ax=ax)
    ax.axvline(0, linestyle="--")
    plt.savefig(os.path.join(RESULTS_DIR, "pairwise.png"), bbox_inches="tight", dpi=300)


def make_bayes_figures():
    full_filename = os.path.join(CONVENTIONAL_ESTIMATES_DIR, "penn.csv")
    columns = [
        col for col in pd.read_csv(full_filename).columns[1:] if col != "Control"
    ]
    conventional_results = Improper.from_csv(full_filename, columns=columns).fit()
    results = Normal.from_csv(full_filename, columns=columns).fit()
    nonparametric_results = Nonparametric.from_csv(full_filename, columns=columns).fit()

    fig, ax = plt.subplots()
    results.line_plot(0, ax=ax)
    plt.savefig(os.path.join(RESULTS_DIR, "bayes_update.png"), bbox_inches="tight", dpi=300)

    fig, axes = plt.subplots(4, figsize=(7, 15), sharex=True)
    conventional_results.point_plot(ax=axes[0], title="OLS estimates")
    results.point_plot(ax=axes[1], title="Parametric empirical Bayes")
    results.point_plot(robust=True, ax=axes[2], title="Parametric empirical Bayes with robust CIs")
    nonparametric_results.point_plot(ax=axes[3], title="Nonparametric empirical Bayes")
    plt.savefig(os.path.join(RESULTS_DIR, "penn.png"), bbox_inches="tight", dpi=300)

def make_rank_figures():
    full_filename = os.path.join(CONVENTIONAL_ESTIMATES_DIR, "penn.csv")
    columns = [
        col for col in pd.read_csv(full_filename).columns[1:] if col != "Control"
    ]
    fig, axes = plt.subplots(2, figsize=(7, 15), sharex=True)

    results = Normal.from_csv(full_filename, columns=columns).fit()
    results.rank_point_plot(alpha=.2, ax=axes[0], title="Parametric empirical Bayes rankings")

    results = Nonparametric.from_csv(full_filename, columns=columns).fit()
    results.rank_point_plot(alpha=.2, ax=axes[1], title="Nonarametric empirical Bayes rankings")

    plt.savefig(os.path.join(RESULTS_DIR, "penn_rankings.png"), bbox_inches="tight", dpi=300)

if __name__ == "__main__":
    np.random.seed(0)
    sns.set()
    make_qvalues_figure()
    make_simultaneous_ci_figure()
    make_pairwise_figure()
    make_bayes_figures()
    make_rank_figures()
