from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Illustrating multiple inference techniques for social science data.',
    author='Dillon Bowen',
    license='MIT',
)
