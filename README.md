A social scientist's guide to multiple inference analysis
=========================================================

Illustrating multiple inference techniques for social science data.

Code available [here](https://gitlab.com/dsbowen/multiple-inference-paper/). Data available [here](https://osf.io/fycgm/). Please note that the OSF repository is for data only and does not contain the code files necessary to analyze them.

## Environment

Click [here](https://gitpod.io/#https://gitlab.com/dsbowen/multiple-inference-paper/-/tree/master/) to run a pre-configured cloud environment with Gitpod.

If working locally, you'll need python-3.8 or higher. Set up your environment with:

```
$ pip install -r requirements.txt
```

## Data

Download the data [here](https://osf.io/fycgm/). Put the `data` folder in the root directory of this repository (i.e., in the same folder as this README file).

The cleaned data should already be in this folder. To re-run the cleaning code, make sure you have all the external data files in `data/external` (see below for data sources) and run:

```
$ make data
```

This command runs the following files in `src`:

- `clean.py` cleans the raw data
- `compute_conventional_estimates.py` computes the conventional estimates (usually by OLS)

The resulting files will be in `data/conventional_estimates` and `data/processed`.

## Main analyses

Run the main analyses with:

```
$ make analyze
```

This command runs the following files in `src`:

- `analyze.py` runs the main analyses for all datasets
- `figures.py` generates additional explanatory figures

The resulting analyses will be in `results/analysis`.

Data sources
------------

[Data](https://osf.io/vnuwb/) and [writeup](https://osf.io/preprints/socarxiv/j2gbn/) for the "judges" dataset. The relevant data file is "FinalDataset_2001-2019.csv". Make sure to UTF-8 encode this file before processing. Put this in `data/external/judges/FinalDataset_2001-2019.csv`.

[Data](https://osf.io/xf86q/) and [writeup](https://forum.effectivealtruism.org/posts/mNRNWkFBZ2K6SHD8a/most-students-who-would-agree-with-ea-ideas-haven-t-heard-of) for the "predictive traits" dataset. Put this in `data/external/predictive_traits/student_values_survey_2022_NYU_anonymized.csv`.

[Data](https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/BPCDH5) for the "superforecasters" dataset. The relevant files are "ifps.csv" and "survey fcasts.yr1.tab". Download both in `.csv` format. Make sure to UTF-8 encode these files. Put these in `data/external/superforecasters/ifps.csv` and `data/external/superforecasters/survey_fcasts.yr1.csv`.

[Writeup](https://www.nber.org/system/files/working_papers/w29055/w29055.pdf) for the "state representatives" dataset. I manually input the data from Figure 4. Also see [this tweet](https://twitter.com/i/status/1419785629236346885). Put this in `data/external/state_reps/state_reps.csv`.

[Data](https://osf.io/rn8tw/?view_only=546ed2d8473f4978b95948a52712a3c5) for the Walmart flu megastudy dataset. The relevant files are "james_stein/dec_estsmat_js.csv.xlsx" and "james_stein/dec_vcov_js.csv" [here](https://osf.io/k8cy5/?view_only=546ed2d8473f4978b95948a52712a3c5) and "Code & Data/Data files/aggregate_data.xlsx", "Code & Data/Data files/WMT_Attributes_Analysis_Data1and3mo.csv", and "Code & Data/Data files/WMT_PI_Clean.csv" [here](https://osf.io/n8su7/?view_only=546ed2d8473f4978b95948a52712a3c5). Put these in `data/external/walmart`. Make sure to UTF-8 encode these files. 

[Summary statistics](https://osf.io/zqbg2/?view_only=c491df37a33840abbdedda4e60176f34) and [aggregate data](https://osf.io/r385j?view_only=c491df37a33840abbdedda4e60176f34) for the Penn-Geisinger flu megastudy dataset. Put these in `data/external/penn`.

[Data](https://osf.io/chk2a) for the personality dataset. Not referenced in paper.

[Data](https://osf.io/9s6mc/?view_only=8bb9282111c24f81a19c2237e7d7eba3) for the exercise study. Put `Data/SetupUp Data/pptdata.csv` in `data/external/exercise`.

Diversity study data provided by the author, Prof. Katy Milkman. Put this in `data/external/20140623_Shareable.csv`.